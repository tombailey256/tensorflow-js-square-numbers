# tensorflow-js-square-numbers

## Introduction

Attempt to learn and plot the function for square numbers (y=x^2) using a neural network built with TensorFlow JS.

## How to run it

 * Load index.html in an ES8 compatible browser
 * Wait for the building, trained and plotting of the model
 * Profit???
